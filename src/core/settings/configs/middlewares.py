# noinspection PyUnresolvedReferences
from core.settings import env

MIDDLEWARE = (
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    # "ai_api.middleware.ApiMiddleware",
    # "ai_logging.middleware.TrackingMiddleware",
)
